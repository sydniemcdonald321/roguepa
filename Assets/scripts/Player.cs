﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject
{
	
	public Text healthText;

	private Animator animator;
	private int playerHealth;
	private int attackPower = 1; 
	private int healthPerSoda = 10;
	private int secondsUntilNextLevel = 1;
	
	protected override void Start()
	{
		base.Start();
		animator = GetComponent<Animator>();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Health: " + playerHealth;
	}
	
	private void OnDisable()
	{
		GameController.Instance.playerCurrentHealth = playerHealth;
	}
	
	void Update()
	{
		if (!GameController.Instance.isPlayerTurn)
		{
			return;
		}
		
		CheckIfGameOver();
		
		int xAxis = 0;
		int yAxis = 0;
		
		xAxis = (int)Input.GetAxisRaw("Horizontal");
		yAxis = (int)Input.GetAxisRaw("Vertical");
		
		if (xAxis != 0)
		{
			yAxis = 0;
		}
		
		if (xAxis != 0 || yAxis != 0)
		{
			playerHealth--;
			healthText.text = "Health: " + playerHealth;
			Move<Wall>(xAxis, yAxis);
			GameController.Instance.isPlayerTurn = false;
		}
	}
	
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if (objectPlayerCollidedWith.tag == "Exit")
		{
			Invoke("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}
		

	}
	
	private void LoadNewLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
	
	protected override void HandleCollision<T>(T component)
	{
		Wall wall = component as Wall; 
		animator.SetTrigger("playerAttack");
		wall.DamageWall(1);
	}
	
	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + "Health\n " + "Health: " + playerHealth;
		animator.SetTrigger("playerHurt");
	} 
	
	private void CheckIfGameOver()
	{
		if(playerHealth <= 0)
		{
			GameController.Instance.GameOver();
		}
	}
}